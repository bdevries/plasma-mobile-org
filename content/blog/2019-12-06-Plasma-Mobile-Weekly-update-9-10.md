---
author: Plasma Mobile team
created_at: 2019-12-06 15:25:00 UTC+2
date: "2019-12-06T00:00:00Z"
title: 'Plasma Mobile: weekly update: part 9-10'
---

The Plasma Mobile team is happy to present the blogpost with updates from week 9 and 10.

## Applications

### Calindori

Calindori, the calendar application, now [offers a flat event view](https://invent.kde.org/kde/calindori/commit/2ec3f541a848420e8a90058695a0007001fa73a2) which allows to show all events in single card list view. The [events are sorted by start date](https://invent.kde.org/kde/calindori/commit/13b3a93bd36d04ce75679615298c7cf26d1371ac).

![Calindori showing flat event view](/img/screenshots/calindori_201912.png)

### Kaidan

Kaidan, the Jabber/XMPP client, has had multiple changes merged, [including a branch from pasnox to overhaul multimedia messages](https://invent.kde.org/kde/kaidan/merge_requests/355). This new change allows to:

* Send voice and video messages from within the application.
* Take photos from within the application.
* Share your location in chat messages.
* Play videos and sounds directly in the chats.

Kaidan also [gained the capability to search through messages](https://invent.kde.org/kde/kaidan/merge_requests/404).

### Spacebar

Jonah Brüchert merged several improvements in Spacebar, the application used to send SMS messages.

* Use [unicode emojis instead of a custom emoji picker](https://invent.kde.org/kde/spacebar/commit/b6d8f543762ad1637e5313f17bd07a8a3030dcaf).
* Added a [loading animation in the chats page](https://invent.kde.org/kde/spacebar/commit/b08ad616218eb973ee8afcc48aed15c77d232d20).
* Fix display of phone numbers and contact names. Previously in some cases just Javascript's `undefined` was displayed.
* Some general visual improvements on the conversation page. There is more space for messages and the input field has a more chat-app like design.

Spacebar was updated in postmarketOS by Bhushan Shah so that SMS now works correctly on supported devices.

### Plasma Phonebook

Nicolas Fella committed a change to [sort the contacts list and add section delegates](https://invent.kde.org/kde/plasma-phonebook/commit/38a52c7e0dfcba389f52d9f0c873ecd38d868cac). [It now also shows the fallback avatar if a contact doesn't have a picture](https://invent.kde.org/kde/plasma-phonebook/commit/44a336c04684fe86eab2ef62c4abf2484996223d).

![Plasma Phonebook section delegates](/img/screenshots/plasma-phonebook.png){: .blog-post-image-small}

### Plasma Settings

Aleix Pol committed a change to [save settings before closing a page](https://invent.kde.org/kde/plasma-settings/commit/2b9ad2da7bde81512bbe7ad420024d6cd5fe4298) as well as a [change to call ::load on the configuration module](https://invent.kde.org/kde/plasma-settings/commit/1fa66624379c841885fb6b49b6c3a0da08f9ef3f). These two changes fix the translations KCM on mobile.

![Translated discover application on PlaMo](/img/screenshots/plamo-discover-translated.jpg){: .blog-post-image-small}

## Want to be part of it?

Next time your name could be here! To find out the right task for you, from promotion to core system development, check out [Find your way in Plasma Mobile](https://www.plasma-mobile.org/findyourway). We are also always happy to welcome new contributors on our [public channels](https://www.plasma-mobile.org/join). See you there!
